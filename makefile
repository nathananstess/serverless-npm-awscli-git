VERSION = latest
IMAGE ?= nathan829/serverless-npm-awscli-git:$(VERSION)
TAG = $(VERSION)

build:
	docker build -t $(IMAGE) .

pull:
	docker pull $(IMAGE)

push:
	docker push $(IMAGE)

tag:
	git tag $(TAG)
	git push origin $(TAG)

# Telling the makefile they are commands, not targets
.PHONY: build pull tag